/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENTSTORAGE;


import BUSINESSLOGIC.Complain;
import BUSINESSLOGIC.Crime;
import BUSINESSLOGIC.FIR;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.*;
import java.sql.Statement;
import java.util.*;


/**
 *
 * @author ASIF QAISRANI
 */
public class Dbhandler{
    private Date date;
    private Connection con;
    private boolean status=false;
    private ArrayList al=new ArrayList();
    private ArrayList outeral=new ArrayList();
   
    ////////////////////////////////
    public ArrayList ListofActiveDutyOfficers(Date date)
    {
  //      String dat=date.toString();
   //     java.sql.Date systemDate=java.sql.Date.valueOf("2013-11-25");
      try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "SELECT * FROM APP.POLICEOFFICER WHERE APP.POLICEOFFICER.DUTY is not NULL AND  APP.POLICEOFFICER.DUTYDATE = '"+date+"'"; 
        ResultSet rs = stmt.executeQuery( SQL );
  
        while(rs.next())
       {
       al.add(rs.getString("NAME"));
       al.add(rs.getString("CNIC"));
       al.add(rs.getString("JOBID"));
       al.add(rs.getString("CONTACT"));
       al.add(rs.getString("EMAIL"));
       al.add(rs.getString("ADDRESS"));
       al.add(rs.getString("DUTYDATE"));
       al.add(rs.getString("DUTY"));
       al.add(rs.getString("Investigating"));       
       outeral.add(al);
            al.clear();
       } 
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
      return outeral;
     }
    public ArrayList GetListOfFIR(Date dat)
    {
    //    String dat=date.toString();
   //     Date systemDate=Date.valueOf(dat);
      try{
        String host="jdbc:oracle:thin:@localhost:1521:XE [system on SYSTEM]";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "SELECT * FROM APP.FIR WHERE APP.FIR.DATE = '"+dat+"'"; 
        ResultSet rs = stmt.executeQuery( SQL );
  
        while(rs.next())
       {
       al.add(rs.getString("FIRID"));
       al.add(rs.getString("DATE"));
       al.add(rs.getString("TIME"));
       al.add(rs.getString("PLACEOFINCIDENT"));
       al.add(rs.getString("DETAILS"));
       al.add(rs.getString("PETITIONERCNIC"));
       outeral.add(al);
       al.clear();
       } 
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
      return outeral;
        //get here the todays reg firs and return
    }
    public ArrayList FirDetails(String FIRID)
    {
             try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "SELECT * FROM APP.FIR where app.firid = '"+FIRID+"'"; 
        ResultSet rs = stmt.executeQuery( SQL );
  
        while(rs.next())
       {
       al.add(rs.getString("FIRID"));
       al.add(rs.getString("DATE"));
       al.add(rs.getString("TIME"));
       al.add(rs.getString("PLACEOFINCIDENT"));
       al.add(rs.getString("DETAILS"));
       al.add(rs.getString("PETITIONERCNIC"));
       } 
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
        // retrieve the detaols of an fir and return
        return al;
    }
    public ArrayList specificPolicStation()
    {
        String incidentPlace="NULL";
       try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "SELECT * FROM APP.FIR "; 
        ResultSet rs = stmt.executeQuery( SQL );
  
       incidentPlace=rs.getString("PLACEOFINCIDENT"); 
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }   
      try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "SELECT * FROM APP.POLICESTATION where app.policestation.place = '"+incidentPlace+"'"; 
        ResultSet rs = stmt.executeQuery( SQL );
  
        al.add(rs.getString("AREACODE"));
        al.add(rs.getString("POLICESTATIONNAME")); 
      
      }
        catch(SQLException er)
       {
           System.out.println(er.getMessage());
       }    
        // retrieve the location from police station details and return
        return al;
    }
    public boolean AssignInvestigation(String FIRID,String officerID)
    {
 try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "UPDATE APP.POLICEOFFICER set app.policeofficer.Assignedfirnum='"+FIRID+"' where cnic='"+officerID+"'"; 
        ResultSet rs = stmt.executeQuery( SQL );
        }
        catch(SQLException er)
       {
           System.out.println(er.getMessage());
       }    
        // do query and chage the status of an officer
        return true;
    }
    public ArrayList FreeOfficers()
    {
      try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "SELECT * FROM APP.POLICEOFFICER WHERE APP.POLICEOFFICER.ASSIGNEDFIRNUM is NULL AND APP.DUTYSTATUS='1' "; 
        ResultSet rs = stmt.executeQuery( SQL );
  
        while(rs.next())
       {
       al.add(rs.getString("NAME"));
       al.add(rs.getString("CNIC"));
       al.add(rs.getString("JOBID"));
       al.add(rs.getString("CONTACT"));
       al.add(rs.getString("EMAIL"));
       al.add(rs.getString("ADDRESS"));
       al.add(rs.getString("DUTYDATE"));
       al.add(rs.getString("DUTY"));
       al.add(rs.getString("Investigating"));       
       outeral.add(al);
         al.clear();   
       } 
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
      return outeral;
    }
    public ArrayList GetDutyList(Date date)
    {
         try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "SELECT * FROM APP.POLICEOFFICER WHERE APP.POLICEOFFICER.DUTYSTATUS='0' "; 
        ResultSet rs = stmt.executeQuery( SQL );
  
        while(rs.next())
       {
       al.add(rs.getString("NAME"));
       al.add(rs.getString("CNIC"));
       al.add(rs.getString("JOBID"));
       al.add(rs.getString("CONTACT"));
       al.add(rs.getString("EMAIL"));
       al.add(rs.getString("ADDRESS"));
       al.add(rs.getString("DUTYDATE"));
       al.add(rs.getString("DUTY"));
       al.add(rs.getString("Investigating"));       
       outeral.add(al);
        al.clear();    
       } 
       }
       catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
      return outeral;
       // give query here to get the previous day duties list
    }
    public boolean AssignDuty(Date date,String OfficerID)
    {
         try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "UPDATE APP.POLICEOFFICER set app.policeofficer.DUTYDATE='"+date+"' where app.policeofficer='"+OfficerID+"'"; 
        ResultSet rs = stmt.executeQuery( SQL );
         }
        catch(SQLException er)
       {
           System.out.println(er.getMessage());
       }    
        //do some query here 
        return true;
        
    }
    public ArrayList getAllOfficersData()
    {
        String dat=date.toString();
        java.sql.Date systemDate=java.sql.Date.valueOf(dat);
      try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "SELECT * FROM APP.POLICEOFFICER"; 
        ResultSet rs = stmt.executeQuery( SQL );
        
        while(rs.next())
       {
       al.add(rs.getString("NAME"));
       al.add(rs.getString("CNIC"));
       al.add(rs.getString("JOBID"));
       al.add(rs.getString("CONTACT"));
       al.add(rs.getString("EMAIL"));
       al.add(rs.getString("ADDRESS"));
       al.add(rs.getString("DUTYDATE"));
       al.add(rs.getString("DUTY"));
       al.add(rs.getString("Investigating"));       
       outeral.add(al);
       al.clear();
       } 
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
      return outeral;
        //apply qury to get all officers records
    }
    public boolean AddnewOfficer(String name,int CNIC,String jobiD,String Contact,String email,String address,String dutydate,String Duty)
    {
         try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "INSERT INTO app.policeofficer(NAME,CNIC,JOBID,CONTACT,EMAIL,ADDRESS,DUTYDATE,DUTYSTATUS) values('name','CNIC', 'jobiD', 'Contact','email','address','dutydate', 'Duty')"; 
        ResultSet rs = stmt.executeQuery( SQL ); 
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
        //apply query here
        return status;
    }
    public boolean UpdateInformation(String name,int CNIC,String jobiD,String Contact,String email,String address,String dutydate,String Duty)
    {
                try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "UPDATE app.policeofficer(NAME,CNIC,JOBID,CONTACT,EMAIL,ADDRESS,DUTYDATE,DUTYSTATUS) values('name','CNIC', 'jobiD', 'Contact','email','address','dutydate', 'Duty')"; 
        ResultSet rs = stmt.executeQuery( SQL ); 
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
        //do your query here
        return status;
    }
    public boolean DeleteInformation(String name,int CNIC,String jobiD,String Contact,String email,String address,String dutydate,String Duty)
    {
                try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="admin";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "DELETE FROM app.policeofficer where CNIC='"+CNIC+"'"; 
        ResultSet rs = stmt.executeQuery( SQL );
       }
        catch(SQLException err)
       {
           System.out.println(err.getMessage());
       }
            return status;
    }
    //////////////////////////////////////////////// AQSA ///////////////////////////////////////
    private static Dbhandler instance;
  public static synchronized Dbhandler getInstance()
	{
		if (instance == null)
			instance = new Dbhandler();
                    return instance;
	}
      public boolean makeFIR(String id,String a_name, int team_mem, String o_name, Date date1, String p_name, int p_cnic , String p_address , int P_contact, String crime_name) throws SQLException
     {
          try
          {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
         String sql="insert into SYSTEM.FIR values(?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement prepSt= con.prepareStatement(sql);
                prepSt.setString(1, id);
                prepSt.setString(2, a_name);
                prepSt.setInt(3, team_mem);
                prepSt.setString(4, o_name);
             //   prepSt.setDate(5,date1);
                prepSt.setString(6, p_name);
                prepSt.setInt(7, p_cnic);
                prepSt.setString(8, p_address);
                prepSt.setInt(9,P_contact);
                prepSt.setString(10, crime_name);
  //              prepSt.setString(11, Status);
             prepSt.executeUpdate();
            prepSt.close();


            con.close();
               System.out.println("U have succesfully added this FIR:" +id);
         }
          catch(SQLException ew)
          {
              System.out.println(ew.getMessage());
          }
              return true;

        }
 
          public boolean enterNewComplain(String comp ,  String Petitioner_name , String Petitioner_CNIC , String p_Address, int P_contactNo , String Accuse_Name , String CrimeType, String Date) throws SQLException
         {
              try
              {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
            String sql="insert into SYSTEM.COMPLAIN values(?,?,?,?,?,?,?,?)";

            PreparedStatement prepSt= con.prepareStatement(sql);
               prepSt.setString(1, comp);
                prepSt.setString(2, Petitioner_name);
                prepSt.setString(3, Petitioner_CNIC);
                 prepSt.setString(4, p_Address);
                prepSt.setInt(5, P_contactNo);
                prepSt.setString(6, Accuse_Name);
                prepSt.setString(7, CrimeType);
                prepSt.setString(8, Date);
             prepSt.executeUpdate();
            prepSt.close();
            con.close();
             }
              catch(SQLException ew)
              {
                       System.out.println(ew.getMessage());
              }
            return true;
            }
 public ArrayList<Complain> Searchcomplain(ArrayList<Complain> alter) throws SQLException
    {
      Complain com = new Complain();
        String p=null;
           try
           {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           String sql="select * from SYSTEM.COMPLAIN";

           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();

              while(executeQuery.next())
         {
               //   al.add(0, com);
                //   alter.clear();
         com.setComplain_ID(executeQuery.getString("ID"));
         com.petitionerName(executeQuery.getString("PETITIONERNAME"));
         com.petitionerCNIC(executeQuery.getString("PETITIONERCNIC"));
         com.petitioneraddress(executeQuery.getString("PETITIONERADDRESS"));
         com.petitionerContact(executeQuery.getInt("PETITIONERCONTACTNO"));
         com.setAccuse_Name(executeQuery.getString("ACCUSENAME"));
         com.crimeName(executeQuery.getString("CRIMETYPE"));
         com.setDate(executeQuery.getString("NEWDATE"));
         alter.add(com);
         }
            System.out.println("U have succesfully view this complain:");
            prepSt.close();
            con.close();
        }
           catch(SQLException ew)
           {
               System.out.println(ew.getMessage());
           }
       // System.out.println("    " +alter.get(0).getComplain_ID());
            return alter;
    }
     public ArrayList<FIR> SearchFIR(ArrayList<FIR> fir) throws SQLException
    {
         FIR firobj = new FIR();
         int index = 0;
        String p=null;
         String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           try
           {
           String sql="select * from SYSTEM.FIR";
           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();
         
         while(executeQuery.next())
         {
             firobj.setFIRid(executeQuery.getString("ID"));
              firobj.setAccuseName(executeQuery.getString("ACCUSENAME"));
              firobj.setInvestiTeamMember(executeQuery.getInt("TEAMMEMBER"));
              firobj.setInvestiOfficerName(executeQuery.getString("OFFICERNAME"));
              firobj.setDatOfInceident(executeQuery.getDate("10.12.2110"));
              firobj.setPetitionerName(executeQuery.getString("PETITIONERNAME"));
              firobj.setPetitionerCNIC(executeQuery.getInt("PETITIONERCNIC"));
              firobj.setPetitionerAddress(executeQuery.getString("PETITIONERADDRESS"));
              firobj.setPetitionerContactNo(executeQuery.getString("PETITIONERCONTACTNO"));
              firobj.setCrimeType(executeQuery.getString("CRIMETYPE"));
            // fir.add(al);
             fir.add(firobj);
//             System.out.println(fir.get(index).getCrimeName());
        
         index++;
             
          }
            System.out.println("U have succesfully view this complain:");
            prepSt.close();
            con.close();
           
        }
           catch(SQLException ew)
           {
               System.out.println(ew.getMessage());
               
           }
            return fir;
    }

    public String checkvali(String vali) throws SQLException
    {
          String p1=null;
        try
        {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);

           String sql="select CRIMEID from SYSTEM.CRIMEVALIDATION where CRIMETYPE = '"+vali+"'";
         PreparedStatement prepSt= con.prepareStatement(sql);
          ResultSet executeQuery = prepSt.executeQuery();
         // System.out.println("HEreeeeeeeeeeeeeee43334435565664eeeeeeee are    uuuu" +executeQuery);
          
         while(executeQuery.next())
         {
         int id1= executeQuery.getInt("CRIMEID");
         p1 = "    This Crime id: " + id1 + "  is Valid so you can change this Complain into FIR  " ;

          }
             System.out.println(p1);
            prepSt.close();
            con.close();
        }
        catch(SQLException ew)
        {
                 System.out.println(ew.getMessage());
        }
            return p1;
    }
    
    public ArrayList<Crime> confirmFIR(ArrayList<Crime> cr) throws SQLException
    {
        Crime c = new Crime();
        boolean Status = false;
        String idi = null;
        try
        {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           String sql="select * from SYSTEM.CHECKVALIDATION";

           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();
             while(executeQuery.next())
         {
            c.setCrime_ID(executeQuery.getInt("CRIMEID"));
            c.setCrime_name(executeQuery.getString("CRIMETYPE"));
            cr.add(c);
         }
          
        }
        catch(SQLException ew)
        {
           System.out.println(ew.getMessage());
           
        }
        return cr;
           
    }
    public boolean ChangeFIRStatus(String ID,String Status )
    {
        boolean ss = false;
        try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "UPDATE SYSTEM.FIR set SYSTEM.FIR.STATUS='"+Status+"' where ID='"+ID+"'";
        ResultSet rs = stmt.executeQuery( SQL );
        ss = true;
        }
       catch(SQLException er)
       {
           System.out.println(er.getMessage());
           ss = false;
       }
        // do query and chage the status of an officer
        return ss;
    }

     public ArrayList searchFIR1() throws SQLException
    {
         ArrayList al = new ArrayList();
         ArrayList alter = new ArrayList();
         try
         {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           String sql="select * from SYSTEM.FIR";

           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();
             String p=null;
              while(executeQuery.next())
         {
         al.add(executeQuery.getString("ID"));
         al.add(executeQuery.getString("ACCUSENAME"));
         al.add(executeQuery.getInt("TEAMMEMBER"));
         al.add(executeQuery.getString("OFFICERNAME"));
         al.add(executeQuery.getString("NEWDATE"));
         al.add(executeQuery.getString("PETITIONERNAME"));
         al.add(executeQuery.getString("PETITIONERCNIC"));
         al.add(executeQuery.getString("PETITIONERADDRESS"));
         al.add(executeQuery.getInt("PETITIONERCONTACTNO"));
         al.add(executeQuery.getString("CRIMETYPE"));
        
         alter.add(al);
         al.clear();
          }
            System.out.println("U have succesfully view this complain:");
            prepSt.close();
            con.close();
        }catch(SQLException ew)
         {
             System.out.println(ew.getMessage());
        }
            return alter;
        }
    
    
}