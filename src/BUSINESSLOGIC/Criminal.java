/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;


import PERSISTENTSTORAGE.DataHandler;
import java.util.ArrayList;

/**
 *
 * @author ASIF QAISRANI
 */
public class Criminal {
   private String FirstName;
   private String LastName;
   private String FatherName;
   private String Address;
     private int CriminalID;
     private ArrayList<Criminal> CriminalList;
     private DataHandler OBj=DataHandler.getInstance();
             
    public Criminal()
{
    CriminalList= new ArrayList<>();
populateCriminalList();

}
     public Criminal(String fname,String lname,String fathername,String address,int id)
{
    FirstName=fname;
    LastName=lname;
    FatherName=fathername;
    Address=address;
    CriminalID=id;

}
     public void SearchCriminalList(int id)
     {
 //    CriminalList=OBj.SearchList(id);
     }
     public ArrayList<Criminal> GetdRequireCriminalList()
    {
    return CriminalList;
    }
       public void clearSearchList()
     {
     CriminalList.clear();
     }
     public void populateCriminalList()
     {
  //       CriminalList=OBj.PopCriminalList();
     }
     public void clearCriminalList()
     {
     CriminalList.clear();
     }
     
    public ArrayList<Criminal> GetCriminalList()
    {
    return CriminalList;
    }
     public String Get_Fname()
     {
           return FirstName;
     }
     public int Get_Id()
     {
      return CriminalID;   
     }
       public String Get_Lname()
     {
      return LastName;
     }
         public String Get_FatherName()
     {
      return FatherName;   
     }
           public String Get_Address()
     {
      return Address;   
     }
}

