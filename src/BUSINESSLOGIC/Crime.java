/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package BUSINESSLOGIC;

/**
 *
 * @author ABEEL
 */
public class Crime {
private int Crime_ID;
    private String Crime_name;

    public Crime() {
    }

    public void setCrime_ID(int Crime_ID) {
        this.Crime_ID = Crime_ID;
    }

    public int getCrime_ID() {
        return Crime_ID;
    }

    public void setCrime_name(String Crime_name) {
        this.Crime_name = Crime_name;
    }

    public String getCrime_name() {
        return Crime_name;
    }

}
