/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;

import PERSISTENTSTORAGE.DataHandler;

import java.util.ArrayList;

/**
 *
 * @author ASIF QAISRANI
 */
public class Automobile {
    private String autoid;
   private String enginenumber;
   private String chesisnumber;
   private String startdate;
   private ArrayList<Automobile> AutoList;
   private DataHandler OBj=DataHandler.getInstance();
    public Automobile()
    {
AutoList= new ArrayList<>();
    }
    public Automobile(String a,String b, String c,String d)
{
    autoid=a;
    enginenumber=c;
    chesisnumber=b;
    startdate=d;


}
   
    
public String get_Autoid()
{
    return autoid;

}
public String get_Enginenumber()
{
   return enginenumber;
}
public String get_Chesisnumber()
{
  return  chesisnumber;
}
public String get_Startdate()
{
    return startdate;
}    
///////////////////Search By Autoid////////////////////////////
  public void SearchById(String id)
     {
     AutoList=OBj.searchByAutoid(id);
     }
     public ArrayList<Automobile> GetAutoDatawithId()
    {
    return AutoList;
    }
       public void clearAutoIdlist()
     {
     AutoList.clear();
     }
///////////////////////By Chesis No///////////////
        public void SearchByChesisNo(String id)
     {
     AutoList=OBj.searchByChesisNo(id);
     }
     public ArrayList<Automobile> GetAutoDatawithChesisNo()
    {
    return AutoList;
    }
       public void clearChesisList()
     {
     AutoList.clear();
     }
       /////////////////////Search By Engine No/////////////////
          public void SearchByEngineNo(String id)
     {
     AutoList=OBj.searchByEngineNo(id);
     }
     public ArrayList<Automobile> GetAutoDatawithEngineNo()
    {
    return AutoList;
    }
       public void clearEngineNoList()
     {
     AutoList.clear();
     }
}
