/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package BUSINESSLOGIC;

/**
 *
 * @author ABEEL
 */
public class Petitioner {
private String Name;
    private String CNIC;
    private String Address;
    private int Contact_NO;

    public Petitioner() {
        this.Name = null;
        this.CNIC = null;
        this.Address=null;
        this.Contact_NO = 0;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public void setCNIC(String CNIC) {
        this.CNIC = CNIC;
    }

    public void setContact_NO(int Contact_NO) {
        this.Contact_NO = Contact_NO;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAddress() {
        return Address;
    }

    public String getCNIC() {
        return CNIC;
    }

    public int getContact_NO() {
        return Contact_NO;
    }

    public String getName() {
        return Name;
    }   
    
}
