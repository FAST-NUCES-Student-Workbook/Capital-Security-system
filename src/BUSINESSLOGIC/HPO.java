/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;
import PERSISTENTSTORAGE.Dbhandler;
import java.sql.SQLException;
import java.util.*;
//import java.sql.Date;

/**
 *
 * @author ASIF QAISRANI
 */
public class HPO {

       private Date date;
               private InvestigationOfficer objInvestOffcr=new InvestigationOfficer();
         private FIRStore objfirstore=new FIRStore();
         private Duties objduty=new Duties();
         private HeadQuarter objHeadQuarter=new HeadQuarter(); //Address, Details, InchargeName, Region);
         private ArrayList al=new ArrayList();
        
         public ArrayList getTodaysRegFirs(Date date)
         {
             objfirstore.getListofFirs(date);           
             return al;
         }         
        public ArrayList getDetailofanFIR(String FIRID)
         {
             al=objfirstore.GetDetails(FIRID);
             return al;  //  return to GUI controller
         }
         public ArrayList determineRelatedPoliceStation()
         {
             al=objfirstore.incidentViewDetails();
             return al;
         }
        public ArrayList GetInvestigationfreeOfficers()
        {
            al=objInvestOffcr.GetFreeInvestigationOfficersList();
            return al;// return this to GUI
        }
         public boolean AssignInvestigation(String FIRID ,String officerID)
         {// this function will be called after the former function GetFreeInvestigationOfficersList()
             boolean status=objfirstore.AssignInvestigation(FIRID,officerID);
             return status; 
         }
          public ArrayList getlistofOnDutyOfficers(Date date)
         {
             al=objHeadQuarter.checkDuties(date);
             return al;
         }
        public ArrayList CheckDutiesforPrevDay(Date date)
         {
             al=objduty.getdutiesfortheDay(date);
             return al;
         }
         public boolean AssignNewDuties(Date date, String OfficerID)
         {
             boolean status=false;
             status=objduty.AssignDuty(date,OfficerID);
             return status;
         }
}
