/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;


import java.awt.List;
import java.sql.SQLException;
import java.util.*;
/**
 *
 * @author M.Abubaker
 */
public class HeadQuarter {
    String Address;
    String Details;
    String InchargeName;
    String Region;
      private Date date;
      private ArrayList al=new ArrayList();
     private Duties objDuties=new Duties();
    public HeadQuarter(String Address, String Details, String InchargeName, String Region) {
        this.Address = Address;
        this.Details = Details;
        this.InchargeName = InchargeName;
        this.Region = Region;
    }
    List fulllist=new List();
    public ArrayList checkDuties(Date date)
    {
        al=objDuties.getlistofActiveDutyOfficers(date);
        //this will return to the gui controller class to display for(){for()}
        return al;
    }
    public HeadQuarter() {
        this.Address ="NULL";
        this.Details ="NULL";
        this.InchargeName ="NULL";
        this.Region ="NULL";
            Newammu=new AmmunationList();
 obj=new AutomobileList();
    }
    
    
    

    public String getAddress() {
        return Address;
    }

    public String getDetails() {
        return Details;
    }

    public String getInchargeName() {
        return InchargeName;
    }

    public String getRegion() {
        return Region;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public void setDetails(String Details) {
        this.Details = Details;
    }

    public void setInchargeName(String InchargeName) {
        this.InchargeName = InchargeName;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }
        private AutomobileList obj;
    private AmmunationList Newammu;
private CriminalList obj1=new CriminalList();

public void addcriminal(String fname,String lname,String fathername,String address,int id)
{
    obj1.InsertCriminal(fname,lname,fathername,address,id);
}
    public void updateCriminal(String fname,String lname,String fathername,String address,int id) throws SQLException
{
    obj1.UpdateCriminal(fname,lname,fathername,address,id);
}
    public ArrayList<Criminal> getCrimList()
    {
        return obj1.GetCriminalRecord();
    }
    public ArrayList<Criminal>getSearchedList(int id)
    {
    return obj1.GetSearchedList(id);   }

///////////////////////Automobile//////////////////
      public ArrayList<Automobile>getIdList(String id)
    {
    return obj.GetAutoIdList(id);   }
       public ArrayList<Automobile>getChesisList(String ChesisNo)
    {
    return obj.GetAutoListByChesisNo(ChesisNo);   }
public ArrayList<Automobile>getEngineList(String EngineNo)
    {
    return obj.GetAutoListByEngineNo(EngineNo);   
    }
public ArrayList<Ammunation>GetAmmunationList(String LisenceNo)
{
    return Newammu.GetAmmunationList(LisenceNo);   
    }
}
