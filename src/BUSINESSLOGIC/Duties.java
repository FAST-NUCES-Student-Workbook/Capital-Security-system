/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;

import PERSISTENTSTORAGE.Dbhandler;
import java.util.*;
//import java.sql.Date;
/**
 *
 * @author M.Abubaker
 */
public class Duties {
    private Date date;
    ArrayList al=new ArrayList();
 //   private DbControllerCSS objDbControl=new DbControllerCSS();
    public Dbhandler inter=new Dbhandler();
    public ArrayList getdutiesfortheDay(Date date)
    {
        al=inter.GetDutyList(date);
        //this is the function which will return the list of duties
        return al;
    }
    public ArrayList getlistofActiveDutyOfficers(Date date)
    {
        al=inter.ListofActiveDutyOfficers(date);
        return al;
    }
    public boolean AssignDuty(Date date,String OfficerID)
    {
        boolean status=false;
        status=inter.AssignDuty(date,OfficerID);
     //   call method to inform the officer here
        return status;
    }
}
