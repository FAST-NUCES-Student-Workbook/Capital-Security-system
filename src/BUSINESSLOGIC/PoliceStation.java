/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author M.Abubaker
 */
public class PoliceStation {
    
    private String PSName;
    private String Address;
    private String AreaCode;

    public PoliceStation(String PSName, String Address, String AreaCode) {
        this.PSName = PSName;
        this.Address = Address;
        this.AreaCode = AreaCode;
    }

    public PoliceStation() {
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public void setAreaCode(String AreaCode) {
        this.AreaCode = AreaCode;
    }

    public void setPSName(String PSName) {
        this.PSName = PSName;
    }

    public String getAddress() {
        return Address;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public String getPSName() {
        return PSName;
    }
    //////////////////////AQSA ////////////////////////////////////////
    
     ArrayList<Complain> al = new ArrayList<Complain>();
     ArrayList<FIR> fr = new ArrayList<FIR>();
    private int index;
    public boolean storeFIR(String id,String a_name, int team_mem, String o_name, Date date1, String p_name, int p_cnic , String p_address , int P_contact, String crimeType, String Status) throws SQLException, Exception
    {
         FIRStore objFIRStore = new FIRStore();
    boolean st = objFIRStore.registerFIR(id, a_name, team_mem, o_name, date1, p_name, p_cnic, p_address, P_contact, crimeType);
    return st;
    }
    public ArrayList<Complain> searchID(String ID) throws SQLException
    {

        int index =0;
        Complain com = null;
     ComplainStore objComplainStore= new ComplainStore();
     al = objComplainStore.searchComplainID(ID);
        return al;
    }

public String storeComplain(String comp ,  String Petitioner_name , String Petitioner_CNIC , String p_Address, int P_contactNo , String Accuse_Name , String CrimeType, String Date) throws SQLException
    {
     ComplainStore objComplainStore= new ComplainStore();
    objComplainStore.registerComplain(comp, Petitioner_name, Petitioner_CNIC, p_Address, P_contactNo, Accuse_Name, CrimeType, Date);
    return comp;
    }

//// ----------------------------------- checkvalidations ----------------- ///
     public String returnName(String id) throws SQLException
    {
         ComplainStore objComplainStore= new ComplainStore();
         return objComplainStore.crimeType(id);
    }
    public boolean checkvali(String vali) throws SQLException
    {
 ComplainStore objComplainStore= new ComplainStore();

         boolean s1 = objComplainStore.checkValidation(vali);
         return s1;
    }
    public ArrayList<FIR> viewFIRAssignOfficer(String Name) throws SQLException
    {
         FIRStore objFIRStore = new FIRStore();
         fr= objFIRStore.searchFIROfficerName(Name);
        return fr;
    }
    public FIR searchFIRID(String ID) throws SQLException
    {
         FIRStore objFIRStore = new FIRStore();
        return objFIRStore.searchFIROnID(ID);
    }
    public ArrayList<Complain> SearchComplains() throws SQLException
    {
        ComplainStore objComplainStore= new ComplainStore();
       return objComplainStore.SearchALL();

    }
    public boolean changeStatus(String id, String Status) throws SQLException
    {
          FIRStore objFIRStore = new FIRStore();
        boolean s1 = objFIRStore.changeFirStatus(id, Status);
        return s1;
    }
    public ArrayList View() throws SQLException
    {
         FIRStore objFIRStore = new FIRStore();
         ArrayList all = objFIRStore.viewALL();
         return all;
    }
}
