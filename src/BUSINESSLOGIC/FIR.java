/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;

import java.sql.Date;

/**
 *
 * @author M.Abubaker
 */
public class FIR {
    private String FIRid;
    private String accuseName;
    private int    investiTeamMember;
    private String investiOfficerName;
    private String petitionerName;
    private int    petitionerCNIC;
    private String petitionerAddress;
    private String petitionerContactNo;
    private String crimeType;
    private Date   datOfInceident;
    private String placeofIncident;
    private String details;

    
    public void setFIRid(String FIRid) {
        this.FIRid = FIRid;
    }

    public void setAccuseName(String accuseName) {
        this.accuseName = accuseName;
    }

    public void setCrimeType(String crimeName) {
        this.crimeType = crimeName;
    }

    public void setDatOfInceident(Date datOfInceident) {
        this.datOfInceident = datOfInceident;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setInvestiOfficerName(String investiOfficerName) {
        this.investiOfficerName = investiOfficerName;
    }

    public void setInvestiTeamMember(int investiTeamMember) {
        this.investiTeamMember = investiTeamMember;
    }

    public void setPetitionerAddress(String petitionerAddress) {
        this.petitionerAddress = petitionerAddress;
    }

    public void setPetitionerCNIC(int petitionerCNIC) {
        this.petitionerCNIC = petitionerCNIC;
    }

    public void setPetitionerContactNo(String petitionerContactNo) {
        this.petitionerContactNo = petitionerContactNo;
    }

    public void setPetitionerName(String petitionerName) {
        this.petitionerName = petitionerName;
    }

    public void setPlaceofIncident(String placeofIncident) {
        this.placeofIncident = placeofIncident;
    }

    public String getFIRid() {
        return FIRid;
    }

    public String getAccuseName() {
        return accuseName;
    }

    public String getCrimeType() {
        return crimeType;
    }

    public Date getDatOfInceident() {
        return datOfInceident;
    }

    public String getDetails() {
        return details;
    }

    public String getInvestiOfficerName() {
        return investiOfficerName;
    }

    public int getInvestiTeamMember() {
        return investiTeamMember;
    }

    public String getPetitionerAddress() {
        return petitionerAddress;
    }

    public int getPetitionerCNIC() {
        return petitionerCNIC;
    }

    public String getPetitionerContactNo() {
        return petitionerContactNo;
    }

    public String getPetitionerName() {
        return petitionerName;
    }

    public String getPlaceofIncident() {
        return placeofIncident;
    }

    public FIR(String FIRid, String accuseName, int investiTeamMember, String investiOfficerName, String petitionerName, int petitionerCNIC, String petitionerAddress, String petitionerContactNo, String crimeName, Date datOfInceident, String placeofIncident, String details) {
        this.FIRid = FIRid;
        this.accuseName = accuseName;
        this.investiTeamMember = investiTeamMember;
        this.investiOfficerName = investiOfficerName;
        this.petitionerName = petitionerName;
        this.petitionerCNIC = petitionerCNIC;
        this.petitionerAddress = petitionerAddress;
        this.petitionerContactNo = petitionerContactNo;
        this.crimeType = crimeName;
        this.datOfInceident = datOfInceident;
        this.placeofIncident = placeofIncident;
        this.details = details;
    }

    public FIR() {
    }

    void setDatOfInceident(java.util.Date date1) {
        setDatOfInceident(date1);
    }

    void setPetitionerContactNo(int Pcontact) {
        setPetitionerContactNo(Pcontact);
    }
}
