/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;
import java.util.*;
//import java.sql.Date;
/**
 *
 * @author M.Abubaker
 */
public class CapitalSecSys {
    
    private HPO objHPOfficer=new HPO();
    private ArrayList al=new ArrayList();
    
    
    public ArrayList getTodaysFIR(Date date)
    {
        al=objHPOfficer.getTodaysRegFirs(date);
        return al;
    }
    public ArrayList  getDetailofanFIR(String FIRID)
    {
        al=objHPOfficer.getDetailofanFIR(FIRID);
        return al;
    }
     public ArrayList determineRelatedPoliceStation()
         {
             al=objHPOfficer.determineRelatedPoliceStation();
             return al;
         }
        public ArrayList GetInvestigationfreeOfficers()
        {
            al=objHPOfficer.GetInvestigationfreeOfficers();
            return al;// return this to GUI
        }
                  public ArrayList getlistofOnDutyOfficers(Date date)
         {
             al=objHPOfficer.getlistofOnDutyOfficers(date);
             return al;
         }
         public boolean AssignInvestigation(String FIRID ,String officerID)
         {// this function will be called after the former function GetFreeInvestigationOfficersList()
             boolean status=objHPOfficer.AssignInvestigation(FIRID,officerID);
             return status; 
         }
        public ArrayList CheckDutiesforPrevDay(Date date)
         {
             al=objHPOfficer.CheckDutiesforPrevDay(date);
             return al;
         }
         public boolean AssignNewDuties(Date date, String OfficerID)
         {
             boolean status=false;
             status=objHPOfficer.AssignNewDuties(date,OfficerID);
             return status;
         }
    
}
