/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package BUSINESSLOGIC;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author ABEEL
 */
public class Dbhandler {

     private static Dbhandler instance;
  private Dbhandler()
  {

  }
  public static synchronized Dbhandler getInstance()
	{
		if (instance == null)
			instance = new Dbhandler();
                    return instance;
	}
      public boolean makeFIR(String id,String a_name, int team_mem, String o_name, String date1, String p_name, String p_cnic , String p_address , int P_contact, String crime_name, String Status) throws SQLException
     {
          try
          {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
         String sql="insert into SYSTEM.FIR values(?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement prepSt= con.prepareStatement(sql);
                prepSt.setString(1, id);
                prepSt.setString(2, a_name);
                prepSt.setInt(3, team_mem);
                prepSt.setString(4, o_name);
                prepSt.setString(5, date1);
                prepSt.setString(6, p_name);
                prepSt.setString(7, p_cnic);
                prepSt.setString(8, p_address);
                prepSt.setInt(9,P_contact);
                prepSt.setString(10, crime_name);
                prepSt.setString(11, Status);
             prepSt.executeUpdate();
            prepSt.close();


            con.close();
               System.out.println("U have succesfully added this FIR:" +id);
         }
          catch(SQLException ew)
          {
              System.out.println(ew.getMessage());
          }
              return true;

        }
 
          public boolean enterNewComplain(String comp ,  String Petitioner_name , String Petitioner_CNIC , String p_Address, int P_contactNo , String Accuse_Name , String CrimeType, String Date) throws SQLException
         {
              try
              {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
            String sql="insert into SYSTEM.COMPLAIN values(?,?,?,?,?,?,?,?)";

            PreparedStatement prepSt= con.prepareStatement(sql);
               prepSt.setString(1, comp);
                prepSt.setString(2, Petitioner_name);
                prepSt.setString(3, Petitioner_CNIC);
                 prepSt.setString(4, p_Address);
                prepSt.setInt(5, P_contactNo);
                prepSt.setString(6, Accuse_Name);
                prepSt.setString(7, CrimeType);
                prepSt.setString(8, Date);
             prepSt.executeUpdate();
            prepSt.close();
            con.close();
             }
              catch(SQLException ew)
              {
                       System.out.println(ew.getMessage());
              }
            return true;
            }
 public ArrayList<Complain> Searchcomplain(ArrayList<Complain> alter) throws SQLException
    {
      Complain com = new Complain();
        String p=null;
           try
           {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           String sql="select * from SYSTEM.COMPLAIN";

           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();

              while(executeQuery.next())
         {
               //   al.add(0, com);
                //   alter.clear();
         com.setComplain_ID(executeQuery.getString("ID"));
         com.petitionerName(executeQuery.getString("PETITIONERNAME"));
         com.petitionerCNIC(executeQuery.getString("PETITIONERCNIC"));
         com.petitioneraddress(executeQuery.getString("PETITIONERADDRESS"));
         com.petitionerContact(executeQuery.getInt("PETITIONERCONTACTNO"));
         com.setAccuse_Name(executeQuery.getString("ACCUSENAME"));
         com.crimeName(executeQuery.getString("CRIMETYPE"));
         com.setDate(executeQuery.getString("NEWDATE"));
         alter.add(com);
         }
            System.out.println("U have succesfully view this complain:");
            prepSt.close();
            con.close();
        }
           catch(SQLException ew)
           {
               System.out.println(ew.getMessage());
           }
       // System.out.println("    " +alter.get(0).getComplain_ID());
            return alter;
    }
     public ArrayList<FIR> SearchFIR(ArrayList<FIR> fir) throws SQLException
    {
         FIR firobj = new FIR();
         int index = 0;
        String p=null;
         String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           try
           {
           String sql="select * from SYSTEM.FIR";
           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();
         
         while(executeQuery.next())
         {
  //           firobj.setFir_id(executeQuery.getString("ID"));
   //           firobj.AccuseName(executeQuery.getString("ACCUSENAME"));
    //          firobj.setTeamMember(executeQuery.getInt("TEAMMEMBER"));
     //         firobj.OfficerName(executeQuery.getString("OFFICERNAME"));
      //        firobj.setDate(executeQuery.getString("NEWDATE"));
       //       firobj.petitionerName1(executeQuery.getString("PETITIONERNAME"));
        //      firobj.petitionerCNIC1(executeQuery.getString("PETITIONERCNIC"));
         //     firobj.petitioneraddress1(executeQuery.getString("PETITIONERADDRESS"));
          //    firobj.petitionerContact1(executeQuery.getInt("PETITIONERCONTACTNO"));
           //   firobj.crimeName1(executeQuery.getString("CRIMETYPE"));
            // fir.add(al);
             fir.add(firobj);
  //          System.out.println(fir.get(index).getCrimeName());
        
         index++;
             
          }
            System.out.println("U have succesfully view this complain:");
            prepSt.close();
            con.close();
           
        }
           catch(SQLException ew)
           {
               System.out.println(ew.getMessage());
               
           }
            return fir;
    }

    public String checkvali(String vali) throws SQLException
    {
          String p1=null;
        try
        {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);

           String sql="select CRIMEID from SYSTEM.CRIMEVALIDATION where CRIMETYPE = '"+vali+"'";
         PreparedStatement prepSt= con.prepareStatement(sql);
          ResultSet executeQuery = prepSt.executeQuery();
         // System.out.println("HEreeeeeeeeeeeeeee43334435565664eeeeeeee are    uuuu" +executeQuery);
          
         while(executeQuery.next())
         {
         int id1= executeQuery.getInt("CRIMEID");
         p1 = "    This Crime id: " + id1 + "  is Valid so you can change this Complain into FIR  " ;

          }
             System.out.println(p1);
            prepSt.close();
            con.close();
        }
        catch(SQLException ew)
        {
                 System.out.println(ew.getMessage());
        }
            return p1;
    }
    
    public ArrayList<Crime> confirmFIR(ArrayList<Crime> cr) throws SQLException
    {
        Crime c = new Crime();
        boolean Status = false;
        String idi = null;
        try
        {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           String sql="select * from SYSTEM.CHECKVALIDATION";

           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();
             while(executeQuery.next())
         {
            c.setCrime_ID(executeQuery.getInt("CRIMEID"));
            c.setCrime_name(executeQuery.getString("CRIMETYPE"));
            cr.add(c);
         }
          
        }
        catch(SQLException ew)
        {
           System.out.println(ew.getMessage());
           
        }
        return cr;
           
    }
    public boolean ChangeFIRStatus(String ID,String Status )
    {
        try{
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con=DriverManager.getConnection(host,uName,uPass);
        Statement stmt = con.createStatement();
        String SQL = "UPDATE SYSTEM.FIR set SYSTEM.FIR.STATUS='"+Status+"' where ID='"+ID+"'";
        ResultSet rs = stmt.executeQuery( SQL );
        }
       catch(SQLException er)
       {
           System.out.println(er.getMessage());
       }
        // do query and chage the status of an officer
        return true;
    }

     public ArrayList searchFIR1() throws SQLException
    {
         ArrayList al = new ArrayList();
         ArrayList alter = new ArrayList();
         try
         {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           String sql="select * from SYSTEM.FIR";

           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();
             String p=null;
              while(executeQuery.next())
         {
         al.add(executeQuery.getString("ID"));
         al.add(executeQuery.getString("ACCUSENAME"));
         al.add(executeQuery.getInt("TEAMMEMBER"));
         al.add(executeQuery.getString("OFFICERNAME"));
         al.add(executeQuery.getString("NEWDATE"));
         al.add(executeQuery.getString("PETITIONERNAME"));
         al.add(executeQuery.getString("PETITIONERCNIC"));
         al.add(executeQuery.getString("PETITIONERADDRESS"));
         al.add(executeQuery.getInt("PETITIONERCONTACTNO"));
         al.add(executeQuery.getString("CRIMETYPE"));
        
         alter.add(al);
         al.clear();
          }
            System.out.println("U have succesfully view this complain:");
            prepSt.close();
            con.close();
        }catch(SQLException ew)
         {
             System.out.println(ew.getMessage());
        }
            return alter;
        }
    /* public ArrayList searchByOfficerName(String officerName)
     {
 try
         {
        String host="jdbc:oracle:thin:@localhost:1521:XE";
        String uName="System";
        String uPass="aqsa";
        Connection con = DriverManager.getConnection(host,uName,uPass);
           String sql="select * from SYSTEM.FIR where OFFICERNAME = '"+officerName+"'";

           PreparedStatement prepSt= con.prepareStatement(sql);
           ResultSet executeQuery = prepSt.executeQuery();
           String p=null;
           while(executeQuery.next())
         {
         al.add(executeQuery.getString("ID"));
         al.add(executeQuery.getString("ACCUSENAME"));
         al.add(executeQuery.getInt("TEAMMEMBR"));
         al.add(executeQuery.getString("OFFICERNAME"));
         al.add(executeQuery.getString("NEWDATE"));
         al.add(executeQuery.getString("PETITIONERNAME"));
         al.add(executeQuery.getString("PETITIONERCNIC"));
         al.add(executeQuery.getString("PETITIONERADDRESS"));
         al.add(executeQuery.getInt("PETITIONERCONTACTNO"));
         al.add(executeQuery.getString("CRIMETYPE"));
         alter.add(al);
         al.clear();
          }
            System.out.println("U have succesfully view this complain:");
            prepSt.close();
            con.close();
        }catch(SQLException ew)
         {
             System.out.println(ew.getMessage());
        }
            return alter;
     }*/
}
