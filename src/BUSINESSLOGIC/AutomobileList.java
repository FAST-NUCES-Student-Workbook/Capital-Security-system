/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;

import BUSINESSLOGIC.Automobile;
import PERSISTENTSTORAGE.DataHandler;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author ASIF QAISRANI
 */
public class AutomobileList {
    
private ArrayList<Automobile> AutoList= new ArrayList<>();
private Automobile NewAuto=new Automobile();
public AutomobileList ()
 {

 }
public String ViewThroughId(String ID) throws SQLException
{
    DataHandler obj=DataHandler.getInstance();
    String result;
result=obj.SearchAutoWithID(ID);
    return result;
}   
 public ArrayList<Automobile> GetAutoIdList(String id)
 {
     NewAuto.clearAutoIdlist();
  NewAuto.SearchById(id);
 return AutoList=NewAuto.GetAutoDatawithId();
 }
 public ArrayList<Automobile> GetAutoListByChesisNo(String id)
 {
     NewAuto.clearChesisList();
  NewAuto.SearchByChesisNo(id);
 return AutoList=NewAuto.GetAutoDatawithChesisNo();
 }
  public ArrayList<Automobile> GetAutoListByEngineNo(String id)
 {
     NewAuto.clearEngineNoList();
  NewAuto.SearchByEngineNo(id);
 return AutoList=NewAuto.GetAutoDatawithEngineNo();
 }
}
