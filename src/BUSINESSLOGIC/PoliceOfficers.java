/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BUSINESSLOGIC;
import PERSISTENTSTORAGE.Dbhandler;
import java.util.*;
/**
 *
 * @author M.Abubaker
 */
public class PoliceOfficers {
    
    
    		  String NAME;
                  int CNIC;
                  String jOBID;
                  String cONTACTNUMBER;
                  String eMIAL;
                  String aDDRESS;
                  Date dUTYDATE;
                  Boolean DUTY;
		  Boolean INVESTIGATING;

    public PoliceOfficers() {
    }
                  
                  
    
 //      private DbControllerCSS objDbControl=new DbControllerCSS();
        public Dbhandler inter=new Dbhandler();
       private ArrayList al=new ArrayList();
       private boolean status;     
       public ArrayList GetOfficersData()
       {
           al=inter.getAllOfficersData();
          return al; 
       }
       public boolean AddnewOfficer(String name,int CNIC,String jobiD,String Contact,String email,String address,String dutydate,String Duty,Boolean investigating)
       {
           status=inter.AddnewOfficer(name,CNIC, jobiD, Contact,email,address,dutydate, Duty);
           return status;
       }
       
       public boolean UpdateInformation(String name,int CNIC,String jobiD,String Contact,String email,String address,String dutydate,String Duty,Boolean investigating)
       {
           status=inter.UpdateInformation(name,CNIC, jobiD, Contact,email,address,dutydate, Duty);
           return true;
       }
       public boolean DeleteInformation(String name,int CNIC,String jobiD,String Contact,String email,String address,String dutydate,String Duty,Boolean investigating)
       {
           status=inter.DeleteInformation(name,CNIC, jobiD, Contact,email,address,dutydate, Duty);
          return true; 
       }

    public PoliceOfficers(String NAME, int CNIC, String jOBID, String cONTACTNUMBER, String eMIAL, String aDDRESS, Date dUTYDATE, Boolean DUTY, Boolean INVESTIGATING, boolean status) {
        this.NAME = NAME;
        this.CNIC = CNIC;
        this.jOBID = jOBID;
        this.cONTACTNUMBER = cONTACTNUMBER;
        this.eMIAL = eMIAL;
        this.aDDRESS = aDDRESS;
        this.dUTYDATE = dUTYDATE;
        this.DUTY = DUTY;
        this.INVESTIGATING = INVESTIGATING;
        this.status = status;
    }

    public int getCNIC() {
        return CNIC;
    }

    public Boolean getDUTY() {
        return DUTY;
    }

    public Boolean getINVESTIGATING() {
        return INVESTIGATING;
    }

    public String getNAME() {
        return NAME;
    }

    public String getaDDRESS() {
        return aDDRESS;
    }

    public ArrayList getAl() {
        return al;
    }

    public String getcONTACTNUMBER() {
        return cONTACTNUMBER;
    }

    public Date getdUTYDATE() {
        return dUTYDATE;
    }

    public String geteMIAL() {
        return eMIAL;
    }

    public Dbhandler getInter() {
        return inter;
    }

    public String getjOBID() {
        return jOBID;
    }

    public boolean isStatus() {
        return status;
    }

    public void setCNIC(int CNIC) {
        this.CNIC = CNIC;
    }

    public void setDUTY(Boolean DUTY) {
        this.DUTY = DUTY;
    }

    public void setINVESTIGATING(Boolean INVESTIGATING) {
        this.INVESTIGATING = INVESTIGATING;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public void setaDDRESS(String aDDRESS) {
        this.aDDRESS = aDDRESS;
    }

    public void setAl(ArrayList al) {
        this.al = al;
    }

    public void setcONTACTNUMBER(String cONTACTNUMBER) {
        this.cONTACTNUMBER = cONTACTNUMBER;
    }

    public void setdUTYDATE(Date dUTYDATE) {
        this.dUTYDATE = dUTYDATE;
    }

    public void seteMIAL(String eMIAL) {
        this.eMIAL = eMIAL;
    }

    public void setInter(Dbhandler inter) {
        this.inter = inter;
    }

    public void setjOBID(String jOBID) {
        this.jOBID = jOBID;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
       
}